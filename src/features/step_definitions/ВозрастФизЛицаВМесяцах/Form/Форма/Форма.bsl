﻿//начало текста модуля

///////////////////////////////////////////////////
//Служебные функции и процедуры
///////////////////////////////////////////////////

&НаКлиенте
// контекст фреймворка Vanessa-Behavior
Перем Ванесса;
 
&НаКлиенте
// Структура, в которой хранится состояние сценария между выполнением шагов. Очищается перед выполнением каждого сценария.
Перем Контекст Экспорт;
 
&НаКлиенте
// Структура, в которой можно хранить служебные данные между запусками сценариев. Существует, пока открыта форма Vanessa-Behavior.
Перем КонтекстСохраняемый Экспорт;

&НаКлиенте
// Функция экспортирует список шагов, которые реализованы в данной внешней обработке.
Функция ПолучитьСписокТестов(КонтекстФреймворкаBDD) Экспорт
	Ванесса = КонтекстФреймворкаBDD;
	
	ВсеТесты = Новый Массив;

	//описание параметров
	//Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,Снипет,ИмяПроцедуры,ПредставлениеТеста,ОписаниеШага,ТипШага,Транзакция,Параметр);

	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ЕстьСотрудникИвановИВТабНомер00001()","ЕстьСотрудникИвановИВТабНомер00001","Допустим Есть сотрудник Иванов И. В., таб. номер 00001","","");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ИспользуемаяДатаРавна(Парам01)","ИспользуемаяДатаРавна","Когда используемая дата равна 13.02.2016","","");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ВозрастСотрудникаРавен(Парам01)","ВозрастСотрудникаРавен","Тогда Возраст сотрудника равен 381","","");
	Ванесса.ДобавитьШагВМассивТестов(ВсеТесты,"ЕстьСотрудникПетровАВТабНомер00002()","ЕстьСотрудникПетровАВТабНомер00002","Допустим Есть сотрудник Петров А. В., таб. номер 00002","","");

	Возврат ВсеТесты;
КонецФункции
	
&НаСервере
// Служебная функция.
Функция ПолучитьМакетСервер(ИмяМакета)
	ОбъектСервер = РеквизитФормыВЗначение("Объект");
	Возврат ОбъектСервер.ПолучитьМакет(ИмяМакета);
КонецФункции
	
&НаКлиенте
// Служебная функция для подключения библиотеки создания fixtures.
Функция ПолучитьМакетОбработки(ИмяМакета) Экспорт
	Возврат ПолучитьМакетСервер(ИмяМакета);
КонецФункции



///////////////////////////////////////////////////
//Работа со сценариями
///////////////////////////////////////////////////

&НаКлиенте
// Процедура выполняется перед началом каждого сценария
Процедура ПередНачаломСценария() Экспорт
	
КонецПроцедуры

&НаКлиенте
// Процедура выполняется перед окончанием каждого сценария
Процедура ПередОкончаниемСценария() Экспорт
	
КонецПроцедуры



///////////////////////////////////////////////////
//Реализация шагов
///////////////////////////////////////////////////

&НаКлиенте
//Допустим Есть сотрудник Иванов И. В., таб. номер 00001
//@ЕстьСотрудникИвановИВТабНомер00001()
Процедура ЕстьСотрудникИвановИВТабНомер00001() Экспорт
	ФизЛицо = НайтиФизЛицоПоКоду("00001");
	Если НЕ ЗначениеЗаполнено(ФизЛицо) Тогда
		ВызватьИсключение "Не найден сотрудник по коду 00001";
	Иначе
		Контекст.Вставить("ФизЛицо",ФизЛицо);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция НайтиФизЛицоПоКоду(Код)
	Возврат Справочники.ФизическоеЛицо.НайтиПоКоду(Код);
КонецФункции

&НаКлиенте
//Когда используемая дата равна 13.02.2016
//@ИспользуемаяДатаРавна(Парам01)
Процедура ИспользуемаяДатаРавна(ПереданнаяДата) Экспорт
	Контекст.Вставить("ТекущаяДата",ПереданнаяДата);
КонецПроцедуры

&НаКлиенте
//Тогда Возраст сотрудника равен 381
//@ВозрастСотрудникаРавен(Парам01)
Процедура ВозрастСотрудникаРавен(ОжидаемыйРезультат) Экспорт
	Результат = РассчитатьВозрастФизЛица(Контекст.ФизЛицо,Контекст.ТекущаяДата);
	Если Результат <> ОжидаемыйРезультат Тогда
		ВызватьИсключение "Результаты отличаются: Ожидаемый: " + ОжидаемыйРезультат + ", текущий: " + Результат;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция РассчитатьВозрастФизЛица(ФизЛицо,ТекущаяДата)	
	Возврат ОбщегоНазначенияСервер.РассчитатьВозрастВМесяцах(ФизЛицо,ТекущаяДата);	
КонецФункции


&НаКлиенте
//Допустим Есть сотрудник Петров А. В., таб. номер 00002
//@ЕстьСотрудникПетровАВТабНомер00002()
Процедура ЕстьСотрудникПетровАВТабНомер00002() Экспорт
	ФизЛицо = НайтиФизЛицоПоКоду("00002");
	Если НЕ ЗначениеЗаполнено(ФизЛицо) Тогда
		ВызватьИсключение "Не найден сотрудник по коду 00002";
	Иначе
		Контекст.Вставить("ФизЛицо",ФизЛицо);
	КонецЕсли;
КонецПроцедуры

//окончание текста модуля